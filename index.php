<!DOCTYPE html>
<html>
<head>
  <meta charset='utf-8' />
  <title>Display a map</title>
  <meta name='viewport' content='initial-scale=1,maximum-scale=1,user-scalable=no' />
  <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.46.0/mapbox-gl.js'></script>
  <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.46.0/mapbox-gl.css' rel='stylesheet' />
  <link href='//mapbox.com/base/latest/base.css' rel='stylesheet' />
  <style>
    body { margin:0; padding:0; }
    #map { position:absolute; top:0; bottom:0; width:100%; }
  </style>
</head>
<body>
<style>
    #menu {
        background: #fff;
        position: absolute;
        z-index: 1;
        top: 10px;
        right: 10px;
        border-radius: 3px;
        width: 120px;
        border: 1px solid rgba(0,0,0,0.4);
        font-family: 'Open Sans', sans-serif;
    }

    #menu a {
        font-size: 13px;
        color: #404040;
        display: block;
        margin: 0;
        padding: 0;
        padding: 10px;
        text-decoration: none;
        border-bottom: 1px solid rgba(0,0,0,0.25);
        text-align: center;
    }

    #menu a:last-child {
        border: none;
    }

    #menu a:hover {
        background-color: #f8f8f8;
        color: #404040;
    }

    #menu a.active {
        background-color: #3887be;
        color: #ffffff;
    }

    #menu a.active:hover {
        background: #3074a4;
    }
</style>

<nav id="menu"></nav>
<div id='map'></div>

<script>

  var hoveredStateId =  null;
  mapboxgl.accessToken = 'pk.eyJ1IjoidGFsaGExMyIsImEiOiJjamxtMzN0YjYxMG5hM2tsNDB3Y2loZ2l6In0.OVx-q_cbEl_xHwf6S9dOtg';

  const map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/talha13/cjljs5d932gg12sqq9q9r8jrj',
    center: [144.9631, -37.8136],
    zoom: 15,
    pitch: 90
  });

  var popup = new mapboxgl.Popup({
        closeButton: false,
        closeOnClick: false
    });

  var toggleableLayerIds = [ 'approved', 'under_construction', 'applied', 'completed'];
  var names = [ 'Approved', 'Under Construction', 'Applied', 'Completed'];
  var colors = [];

  colors["approved"] = '#31f63f';
  colors["under_construction"] = '#ecec36';
  colors["applied"] = '#54e5ed';
  colors["completed"] = '#2a0fd7';

  map.on('load', function(){

    map.on('mouseenter', 'approved', function (e) {
      showPopupOnHover(e);
    });

    map.on('mouseleave', 'approved', function () {
        removePopup();
    });

    map.on('click', 'approved', function(e){
      showPopoupOnClick(e);
    });

    map.on('mouseenter', 'under_construction', function (e) {
        showPopupOnHover(e);
    });

    map.on('mouseleave', 'under_construction', function () {
        removePopup();
    });

    map.on('click', 'under_construction', function(e){
      showPopoupOnClick(e);
    });

    map.on('mouseenter', 'applied', function (e) {
        showPopupOnHover(e);
    });

    map.on('mouseleave', 'applied', function () {
        removePopup();
    });

    map.on('click', 'applied', function(e){
      showPopoupOnClick(e);
    });

    map.on('mouseenter', 'completed', function (e) {
        showPopupOnHover(e);
    });

    map.on('mouseleave', 'completed', function () {
        removePopup();
    });

    map.on('click', 'completed', function(e){
        showPopoupOnClick(e);
    });



for (var i = 0; i < toggleableLayerIds.length; i++) {

    var id = toggleableLayerIds[i];

    var link = document.createElement('a');
    link.href = '#';
    link.className = 'active';
    link.textContent = names[i];
    link.style.backgroundColor = colors[id];
    link.id = id;
    link.style.color = "#000000";

    link.onclick = function (e) {

        var clickedLayer = this.id;
        e.preventDefault();
        e.stopPropagation();

        var visibility = map.getLayoutProperty(clickedLayer, 'visibility');

        if (visibility === 'visible') {
            this.className = '';
            this.style.backgroundColor = '#FFFFFF';
            // this.style.color = "#FFFFFF";
            map.setLayoutProperty(clickedLayer, 'visibility', 'none');
        } else {
            map.setLayoutProperty(clickedLayer, 'visibility', 'visible');
            this.className = 'active';
            this.style.backgroundColor = colors[clickedLayer];
            // this.style.color = "#000000";
        }
    };

    var layers = document.getElementById('menu');
    layers.appendChild(link);
  }

});

  function showPopoupOnClick(e){

      var coordinates = e.features[0].geometry.coordinates[0][0];
      var description = '<h4>' + e.features[0].properties.address + '</h4>';
      description = description +'<br><strong>Permit Number: </strong>'+ e.features[0].properties.permit_num
      description = description +'<br><strong>Number of Floors: </strong>'+ e.features[0].properties.num_floors
      description = description +'<br><strong>Shape Type: </strong>'+ e.features[0].properties.shape_type
      description = description +'<br><strong>Status: </strong>'+ e.features[0].properties.status  

      while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
            coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
        }

      popup.setLngLat(coordinates)
            .setHTML(description)
            .addTo(map);
  }

  function showPopupOnHover(e){

      map.getCanvas().style.cursor = 'pointer';
      var coordinates = e.features[0].geometry.coordinates[0][0];
      console.log(coordinates);
      var description = e.features[0].properties.address;
      
      while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
            coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
        }

      popup.setLngLat(coordinates)
            .setHTML(description)
            .addTo(map);
  }

  function removePopup(){

    map.getCanvas().style.cursor = '';
    popup.remove();
  }

</script>

</body>
</html>